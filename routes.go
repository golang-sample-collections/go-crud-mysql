package go_crud_mysql

/**
 * Created by GoLand.
 * Project : go-crud-mysql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-12-17
 * Time: 16:26
 */

import (
	"net/http"

	"github.com/gorilla/mux"
)

func setStaticFolder(route *mux.Router) {
	fs := http.FileServer(http.Dir("./public/"))
	route.PathPrefix("/public/").Handler(http.StripPrefix("/public/", fs))
}

func addApproutes(route *mux.Router) {

	setStaticFolder(route)

	route.HandleFunc("/", renderHome)

	route.HandleFunc("/user", getUsers).Methods("GET")

	route.HandleFunc("/user", insertUser).Methods("POST")

	route.HandleFunc("/user/{id}", deleteUser).Methods("DELETE")

	route.HandleFunc("/user", updateUser).Methods("PUT")

}
